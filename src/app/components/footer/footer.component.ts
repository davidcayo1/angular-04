import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor() { }

  date: string = new Date().toLocaleDateString();
  showDate = this.date;
  title: string = 'Fecha actual'

  ngOnInit(): void {
  }

}
